
#ifndef COLLECTOR_H
#define COLLECTOR_H

#include <pcap.h>
#include <stdio.h>


#include "workingqueue.h"
#include "packets/ippacket.h"
#include "packets/tcppacket.h"
#include "packets/udppacket.h"
#include "rules/tcppacketrule.h"
#include "rules/udppacketrule.h"
#include "rules/ipsignaturerule.h"
#include "rules/tcpsignaturerule.h"
#include "network.h"
#include "configuration.h"
#include "structs.h"

namespace WorldMapper {

    class Collector {
    public:
        static void run(std::string interface, std::string captureFilter
                , WorkingQueue<WorldMapper::Packet::IPPacket*> *packetQueue, Configuration &config);


    private:

        Collector() {
        }

        /**
         * Handles termination signals
         * @param signo
         */
        static void signalHandling(int signo);

        /**
         * Determines  the type of etherPacket whether if it is IPv4 or IPv6
         * taken from: http://stackoverflow.com/questions/6256821/can-i-use-pcap-library-for-receiving-ipv6-packets
         * @param args pcap callback args
         * @param pkthdr pcap callback pointer to packet header
         * @param p pcap callback pointer to the ether header within the package
         * @return 
         */
        static u_int16_t etherPacket(u_char *args, const struct pcap_pkthdr *pkthdr, const u_char *p);

        /**
         * Open a pcap socket with device and filter
         * @param device The device to listen on
         * @param bpfstr The filter string
         * @return 
         */
        static pcap_t* openPcapSocket(const char* device, const char* bpfstr);

        /**
         * The capturing loop
         * @param pd pointer to the pcap session
         * @param packets limit of packets to read
         * @param func function to call when a packet was captured
         */
        static void captureLoop(pcap_t* pd, int packets, pcap_handler func);

        /**
         * Parses the IPv4 header
         * @param packetptr Pointer to the captured packet
         * @param conn Pointer to the connection where the IP header informations should be saved to
         */
        static void parseIP(const u_char *packetptr, const struct pcap_pkthdr* pkthdr);

        /**
         * Call back function that gets called when a packet was captured
         * @param args call back arguments (forwarded from user parameter of pcap_loop)
         * @param pkthdr pointer the pcap packet header
         * @param p pointer to the ether header
         */
        static void pcapCallback(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* p);


        static pcap_t* _pd;
        static bool _run;
        static std::string _interface;
        static std::string _captureFilter;
        static WorkingQueue<WorldMapper::Packet::IPPacket*> *_packetQueue;
        static int _packets;
        static Configuration _config;
        static int _linkhdrlen;
    };
};

#endif /* COLLECTOR_H */

