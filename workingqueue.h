/* 
 * File:   workingqueue.h
 * Author: Gerald Ortner g.ortner@gmail.com 
 *
 * Created on June 1, 2017, 9:54 PM
 * 
 * Implementation of a thread save queue
 */

#ifndef WORKINGQUEUE_H
#define WORKINGQUEUE_H

#include <pthread.h>
#include <list>
 
using namespace std;
 
template <typename T> class WorkingQueue
{ 
    std::list<T>   queue;
    pthread_mutex_t mutex;
    pthread_cond_t  condv;

public:
    WorkingQueue() {
        pthread_mutex_init(&mutex, NULL);
        pthread_cond_init(&condv, NULL);
    }
    
    ~WorkingQueue() {
        pthread_mutex_destroy(&mutex);
        pthread_cond_destroy(&condv);
    }
    
    /**
     * Adds item to the queue
     * @param item
     */
    void push(T item) {
        pthread_mutex_lock(&mutex);
        queue.push_back(item);
        pthread_cond_signal(&condv);
        pthread_mutex_unlock(&mutex);
    }
    
    /**
     * Removes and returns the next item in the queue
     * @return element of type T
     */
    T pop() {
        pthread_mutex_lock(&mutex);
        while (queue.size() == 0) {
            pthread_cond_wait(&condv, &mutex);
        }
        T item = queue.front();
        queue.pop_front();
        pthread_mutex_unlock(&mutex);
        return item;
    }
    
    /**
     * returns the number of elements in the queue
     * @return size of the queue
     */
    int size() {
        pthread_mutex_lock(&mutex);
        int size = queue.size();
        pthread_mutex_unlock(&mutex);
        return size;
    }
};



#endif /* WORKINGQUEUE_H */

