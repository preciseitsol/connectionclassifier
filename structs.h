/* 
 * File:   structs.h
 * Author: root
 *
 * Created on August 7, 2017, 5:24 PM
 * 
 * Collection of structs
 */

#ifndef STRUCTS_H
#define STRUCTS_H

namespace WorldMapper {

    struct Session {
        std::string identifier;
        int sc; /* 0 Client 1 Server*/
        timeval start;
        timeval updated;
        bool classified;
    };

}

#endif /* STRUCTS_H */

