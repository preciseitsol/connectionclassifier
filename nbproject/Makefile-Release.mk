#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/classifier.o \
	${OBJECTDIR}/collector.o \
	${OBJECTDIR}/configuration.o \
	${OBJECTDIR}/external/SpookyV2.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/network.o \
	${OBJECTDIR}/packets/ippacket.o \
	${OBJECTDIR}/packets/tcppacket.o \
	${OBJECTDIR}/packets/udppacket.o \
	${OBJECTDIR}/rules/ipsignaturerule.o \
	${OBJECTDIR}/rules/tcppacketrule.o \
	${OBJECTDIR}/rules/tcpsignaturerule.o \
	${OBJECTDIR}/rules/torrule.o \
	${OBJECTDIR}/rules/udppacketrule.o \
	${OBJECTDIR}/util.o \
	${OBJECTDIR}/worldmapper.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/connectionclassifier

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/connectionclassifier: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/connectionclassifier ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/classifier.o: classifier.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/classifier.o classifier.cpp

${OBJECTDIR}/collector.o: collector.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/collector.o collector.cpp

${OBJECTDIR}/configuration.o: configuration.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/configuration.o configuration.cpp

${OBJECTDIR}/external/SpookyV2.o: external/SpookyV2.cpp
	${MKDIR} -p ${OBJECTDIR}/external
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/external/SpookyV2.o external/SpookyV2.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/network.o: network.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/network.o network.cpp

${OBJECTDIR}/packets/ippacket.o: packets/ippacket.cpp
	${MKDIR} -p ${OBJECTDIR}/packets
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/packets/ippacket.o packets/ippacket.cpp

${OBJECTDIR}/packets/tcppacket.o: packets/tcppacket.cpp
	${MKDIR} -p ${OBJECTDIR}/packets
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/packets/tcppacket.o packets/tcppacket.cpp

${OBJECTDIR}/packets/udppacket.o: packets/udppacket.cpp
	${MKDIR} -p ${OBJECTDIR}/packets
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/packets/udppacket.o packets/udppacket.cpp

${OBJECTDIR}/rules/ipsignaturerule.o: rules/ipsignaturerule.cpp
	${MKDIR} -p ${OBJECTDIR}/rules
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rules/ipsignaturerule.o rules/ipsignaturerule.cpp

${OBJECTDIR}/rules/tcppacketrule.o: rules/tcppacketrule.cpp
	${MKDIR} -p ${OBJECTDIR}/rules
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rules/tcppacketrule.o rules/tcppacketrule.cpp

${OBJECTDIR}/rules/tcpsignaturerule.o: rules/tcpsignaturerule.cpp
	${MKDIR} -p ${OBJECTDIR}/rules
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rules/tcpsignaturerule.o rules/tcpsignaturerule.cpp

${OBJECTDIR}/rules/torrule.o: rules/torrule.cpp
	${MKDIR} -p ${OBJECTDIR}/rules
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rules/torrule.o rules/torrule.cpp

${OBJECTDIR}/rules/udppacketrule.o: rules/udppacketrule.cpp
	${MKDIR} -p ${OBJECTDIR}/rules
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rules/udppacketrule.o rules/udppacketrule.cpp

${OBJECTDIR}/util.o: util.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/util.o util.cpp

${OBJECTDIR}/worldmapper.o: worldmapper.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/worldmapper.o worldmapper.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
