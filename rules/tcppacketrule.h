/* 
 * File:   TCPPacketRule.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 1:33 AM
 * 
 * Applies the rules to the given packets
 */

#ifndef TCPPACKETRULE_H
#define TCPPACKETRULE_H

#include <vector>

#include "ipacketrule.h"
#include "../packets/tcppacket.h"

namespace WorldMapper {
    namespace Rule {

        class TCPPacketRule : public IPacketRule {
        public:
            TCPPacketRule();
            bool apply(const Packet::IPPacket *packet, classificationMap &classification) const override;
        private:
            std::vector<IPacketRule*> rules_;

        };
    }
}
#endif /* TCPPACKETRULE_H */

