/* 
 * File:   TCPPacketRule.cpp
 * Author: Gerald Ortner g.ortner@gmail.com
 * 
 * Created on June 10, 2017, 1:33 AM
 */
#include<iostream>

#include "tcppacketrule.h"
#include "ipsignaturerule.h"
#include "tcpsignaturerule.h"
#include "torrule.h"

#include <sstream>

WorldMapper::Rule::TCPPacketRule::TCPPacketRule() {
    IPSignatureRule *iprule = new IPSignatureRule();
    iprule->readRules();
    rules_.push_back(iprule);
    TCPSignatureRule *tcprule = new TCPSignatureRule();
    tcprule->readRules();
    rules_.push_back(tcprule);
    TORRules *torrule = new TORRules();
    rules_.push_back(torrule);
}

bool WorldMapper::Rule::TCPPacketRule::apply(const WorldMapper::Packet::IPPacket *packet, classificationMap &classification) const {

    //Sanity check
    WorldMapper::Packet::TCPPacket *tcppacket = (WorldMapper::Packet::TCPPacket *) packet;

    if (tcppacket->getSyn() != 1 || tcppacket->getPacketDirection() == WorldMapper::Packet::PacketDirection::OUT) {
        return false;
    }

    if (packet->getSrcip().isSiteLocal() == true ||
            packet->getSrcip().isMulticast() == true ||
            packet->getSrcip().isLoopback() == true) {
        return false;
    }

    for (const auto &rule : rules_) {
        bool retval = rule->apply(packet, classification);
    }


    //if packet couldn't be classified apply other tag
    if (classification[Category::SERVICE].size() == 0) {
        std::vector<std::string> service{"OTHER"};
        classification[Category::SERVICE].emplace_back(service);
    }
    if (classification[Category::SERVICEPROVIDER].size() == 0) {
        std::vector<std::string> service{"OTHER"};
        classification[Category::SERVICEPROVIDER].emplace_back(service);
    }
    
    if (classification[Category::NETWORKT].size() == 0) {
        std::vector<std::string> service{"OTHER"};
        classification[Category::NETWORKT].emplace_back(service);
    }
    return true;
}

