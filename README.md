# README #

This is project contains the prototype I've developed for my master thesis.
To get it up and running libpcap, MongoDB, and the POCO library (https://pocoproject.org/) need to be installed.

##What does it do?##
Basically it captures network traffic, stores metrics for captured packets and sessions in a database and tries to classify the traffic.
Classification happens according to a new approach where each IP is assigned to a Service and Service-Provider. Where Service does not mean a Layer 7 
applications but Cloud Services like Cloud Computing (Amazon EC2), Cloud Storage (Amazon S3), Content Delivery Networks, TOR (currently only exit nodes), VPN (inbound connections
which have been tunneld through a VPN server to hide identity or bypass service restrictions). Service-Providers are companies providing such services.
Currently CDN, TOR and VPN connection work relyable for most cases. Where CDN detection is based on previously collected data and might become outdated soon.
Cloud Computing and Storage detection is currently only based on static data collected from the Service-Providers.
To improve the features more data need to be collection and new ways for service detection need to be found.

##Placement##
To make it most effective it should be placed in front or right behind a firewall. NAT routers might have some impact on classification relyability.

##Rule Definitions##
Own rules can be added by editing or adding files in ip.rules.d or tcp.rules.d.
IP rules are static rules only based on previously collected data.
TCP rules can be applied to almost all metrics of the IP and TCP header. Currently for TCP rules there is no option to limit them to a certain range of IPs. This is something that needs to added in the future

## License  ##
GPLv2

##Contact##
Gerald Ortner

gerald@preciseit.at