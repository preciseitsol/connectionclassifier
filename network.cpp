#include "network.h"
#include "util.h"
#include <algorithm>
#include <sstream>

std::string WorldMapper::reverseIP(Poco::Net::IPAddress ip) {
    char delim = '.';
    if (ip.family() == Poco::Net::IPAddress::Family::IPv6) {
        delim = ':';
    }
    std::vector<std::string> ips = WorldMapper::split(ip.toString(), delim);
    std::reverse(ips.begin(), ips.end());
    std::stringstream ss;
    for (auto &seg : ips) {
        ss << seg << ".";
    }
    std::string retVal = ss.str();
    //remove trailing .
    retVal.erase(retVal.size() - 1);
    return retVal;
}