#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <map>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/builder/basic/sub_document.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/pool.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/instance.hpp>


#include "packets/tcppacket.h"
#include "packets/udppacket.h"
#include "packets/ippacket.h"
#include "rules/ipacketrule.h"
#include "rules/tcppacketrule.h"
#include "rules/udppacketrule.h"
#include "workingqueue.h"
#include "structs.h"
#include "worldmapper.h"

#define NUM_THREADS    4

namespace WorldMapper {

    class Classifier {

        enum Collection {
            COLTCPV4, COLUDPV4
        };

    public:
        Classifier(WorkingQueue<Packet::IPPacket*> *packetQueue, Configuration &config);
        void run();

    private:
        /**
         * Creates a mongodb document for the tcp specific information
         * @param conn The tcp packet
         * @return the mongodb document value
         */
        bsoncxx::document::value generateTCPSessionSubDocument(Packet::TCPPacket *packet);

        /**
         * Creates a mongodb document for the tcp specific information
         * @param conn The tcp packet
         * @return the mongodb document value
         */
        bsoncxx::document::value generateUDPSessionSubDocument(Packet::UDPPacket *packet);

        /**
         * Updates an entry in the specified collection
         * @param doc New value document
         * @param filter Filter target document to update
         * @param collection Target collection
         */
        void updateOneMongoDB(bsoncxx::stdx::optional<bsoncxx::document::value> doc,
                bsoncxx::stdx::optional<bsoncxx::document::value> filter, Collection collection);

        /**
         * Inserts a new document into the specified collection
         * @param doc Document to insert
         * @param collection Target collection
         * @return OID of the newly inserted document
         */
        bsoncxx::oid insertOneMongoDB(bsoncxx::stdx::optional<bsoncxx::document::value> doc, Collection collection);

        /**
         * Enforces the close of a session (logical)
         * Should be used in case of a Syn while the sessions should be already open
         * or after a timeout
         * @param session The session in question
         * @param collection The collection the session belongs to
         */
        void enforceClose(Session session, Collection collection);

        /**
         * Closes all active sessions in the database
         */
        void enforceCloseAll();

        /**
         * Write packet information to database
         * @param tcppacket The packet information to be written to database
         */
        void writeTCPPacketToMongoDB(Packet::TCPPacket *tcppacket);

        /**
         * Write packet information to database
         * @param tcppacket Packet information to be written to database
         */
        void writeUDPPacketToMongoDB(Packet::UDPPacket *udppacket);

        /**
         * Writes the classification values for the packet to db
         * @param classification The classifications found for the packet
         * @param packet The packet in question
         */
        void classificationToMongo(classificationMap classification, Packet::IPPacket *packet, Collection collection);

        void process();
        static void *processPackets(void *p);


        bool _run;

        typedef std::map<std::string, Session> SessionMap;

        SessionMap sessions;

        mongocxx::instance instance{};
        mongocxx::uri *_uri;
        mongocxx::pool *_pool;

        pthread_t *_threads;
        const int BUFFLEN = 255;


        WorkingQueue<Packet::IPPacket*> *_packetQueue;
        Configuration _config;

        Rule::TCPPacketRule *_tcpRule;
        Rule::UDPPacketRule *_udpRule;
    };
}

#endif /* CLASSIFIER_H */

