/* 
 * File:   SignatureRules.h
 * Author:  Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 3:57 PM
 * 
 * Reads IP based rules from configuration files
 */

#ifndef IPSIGNATURERULE_H
#define IPSIGNATURERULE_H

#include <string>
#include <vector>

#include "ipacketrule.h"
#include "../packets/ippacket.h"
#include "../external/json.hpp"


using json = nlohmann::json;

namespace WorldMapper {
    namespace Rule {

        class IPSignatureRule : public IPacketRule {
        public:
            IPSignatureRule();
            bool apply(const WorldMapper::Packet::IPPacket *packet, classificationMap &classification) const override;
            std::vector<IPSignatureRule> rules;
            void readRules();
        private:
            void readRulesFromFile(std::string path);

        };

        inline void to_json(json& j, const IPSignatureRule& rule) {
            j = json{
                {"ip", rule.ip.toString()},
                //        {"service", rule.service}
            };
        }

        inline void from_json(const json& j, IPSignatureRule& rule) {
            std::string cdir = j.at("ip").get<std::string>();
            int cdirLoc = cdir.find("/");
            rule.ip = Poco::Net::IPAddress(cdir.substr(0, cdirLoc));
            int netmask = stoi(cdir.substr(cdirLoc + 1, cdir.length()));
            rule.netmask = Poco::Net::IPAddress(netmask, rule.ip.family());
            std::vector < std::string > empty;
            std::vector < std::string > sp = j.value<std::vector < std::string >> ("serviceprovider", empty);
            if(sp.size() != 0){
                rule.classifications[Category::SERVICEPROVIDER] = sp;
            }
            std::vector < std::string > s = j.value<std::vector < std::string >> ("service", empty);
            if(s.size() != 0){
                rule.classifications[Category::SERVICE] = s;
            }
        }


    }
}
#endif /* IPSIGNATURERULE_H */

