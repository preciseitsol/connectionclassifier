/* 
 * File:   SignatureRules.h
 * Author:  Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 3:57 PM
 * 
 * Reads TCP packet rules form configuration files
 */

#ifndef TCPSIGNATURERULE_H
#define TCPSIGNATURERULE_H

#include <map>
#include "../external/json.hpp"
#include "ipacketrule.h"
#include "../packets/ippacket.h"

using json = nlohmann::json;
namespace WorldMapper {
    namespace Rule {

        enum TCPRuleOptions {
            SRCPRT, DSTPRT, WSIZE, WSCALE, TCPHL, MSS, SACKP, TSPRESENT
            , LEN, IPHL, IPLEN, TOS, PROTO

        };

        const std::map<std::string, TCPRuleOptions> tcpRuleOptionsMap = {
            {"SRCPRT", SRCPRT},
            {"DSTPRT", DSTPRT},
            {"WINSIZE", WSIZE},
            {"WSCALE", WSCALE},
            {"TCPHL", TCPHL},
            {"MSS", MSS},
            {"SACKP", SACKP},
            {"TSPRESENT", TSPRESENT},
            {"LEN", LEN},
            {"IPHL", IPHL},
            {"IPLEN", IPLEN},
            {"TOS", TOS},
            {"PROTO", PROTO},
        };

        class TCPSignatureRule : public IPacketRule {
        public:
            TCPSignatureRule();
            TCPSignatureRule(const TCPSignatureRule& orig, classificationMap &classification);

            bool apply(const Packet::IPPacket *packet, classificationMap &classification) const override;
            std::map<std::string, std::vector<int>> options;
            void readRules();
            std::vector<TCPSignatureRule> rules;
            WorldMapper::Packet::PacketDirection direction;
        private:
            bool doesMatch(std::vector<int> options, int value) const;
            void readRulesFromFile(std::string path);


        };

        inline void to_json(json& j, const TCPSignatureRule& rule) {
            j = json{
                {"name", rule.name},
                {"ip", rule.ip.toString()},
                //        {"service", rule.classificator[Category.service]},
                {"options", rule.options}
            };
        }

        inline void from_json(const json& j, TCPSignatureRule& rule) {
            auto joptions = j.at("options").get<std::map < std::string, json >> ();
            for (std::map<std::string, json>::iterator it = joptions.begin(); it != joptions.end(); ++it) {
                json o = it->second;
                for (auto& element : o) {
                    std::transform(element.begin(), element.end(), element.begin(), ::toupper);
                    rule.options[it->first].push_back(element);
                }

            }
            rule.name = j.at("name").get<std::string>();
            std::string cdir = j.value("ip", "0.0.0.0/0");

            int cdirLoc = cdir.find("/");
            rule.ip = Poco::Net::IPAddress(cdir.substr(0, cdirLoc));
            int netmask = stoi(cdir.substr(cdirLoc + 1, cdir.length()));
            rule.netmask = Poco::Net::IPAddress(netmask, rule.ip.family());
            std::vector < std::string > empty;
            rule.direction = static_cast<WorldMapper::Packet::PacketDirection> (j.value("direction", 2));
            std::vector < std::string > s = j.value<std::vector < std::string >> ("service", empty);
            std::vector < std::string > nt = j.value<std::vector < std::string >> ("link", empty);
            std::vector < std::string > sp = j.value<std::vector < std::string >> ("serviceprovider", empty);
            if (sp.size() != 0) {
                rule.classifications[Category::SERVICEPROVIDER] = sp;
            }
            if (s.size() != 0) {
                rule.classifications[Category::SERVICE] = s;
            }
            if (nt.size() != 0) {
                rule.classifications[Category::NETWORKT] = nt;
            }
        }
    }
}
#endif /* TCPSIGNATURERULE_H */

