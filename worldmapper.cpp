#include "workingqueue.h"

#include "collector.h"
#include "classifier.h"
#include "packets/ippacket.h"
#include "worldmapper.h"

using namespace WorldMapper;
using namespace WorldMapper::Packet;




WorldMapper::WorldMapper::WorldMapper(std::string interface, std::string captureFilter) {
    WorkingQueue<IPPacket*> packetQueue;
    Configuration config;

    Classifier classifier(&packetQueue, config);
    classifier.run();


    Collector::run(interface, captureFilter, &packetQueue, config);
}
