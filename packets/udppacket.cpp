/* 
 * File:   TCPPacket.cpp
 * Author: Gerald Ortner g.ortner@gmail.com
 * 
 * Created on June 10, 2017, 2:06 AM
 */

#include <netinet/udp.h>
#include <sstream>
#include <iostream>

#include "udppacket.h"
#include "../external/SpookyV2.h"

using namespace std;

void WorldMapper::Packet::UDPPacket::process(const u_char *&packetptr, const struct pcap_pkthdr* pkthdr, const WorldMapper::Configuration *configuration) {
    WorldMapper::Packet::IPPacket::process(packetptr, pkthdr, configuration);
    struct udphdr* udph = (struct udphdr*) packetptr;
    this-> udpLen_ = udph->uh_ulen;
    this->srcport_ = udph->uh_sport;
    this->dstport_ = udph->uh_dport;
    //advance pointer to payload;
    packetptr += 8;

}

std::string WorldMapper::Packet::UDPPacket::hash128(bool reversed) {
    uint64 hash1 = 1;
    uint64 hash2 = 2;
    char *inBuf = new char[BUFFLEN]();
    if (reversed) {
        snprintf(inBuf, BUFFLEN, "%s%u%u%s%u", this->dstip_.toString().c_str(), this->dstport_
                , this->proto_, this->srcip_.toString().c_str(), this->srcport_);
    } else {
        snprintf(inBuf, BUFFLEN, "%s%u%u%s%u", this->srcip_.toString().c_str(), this->srcport_
                , this->proto_, this->dstip_.toString().c_str(), this->dstport_);
    }

    SpookyHash::Hash128(inBuf, BUFFLEN, &hash1, &hash2);

    std::stringstream ss;
    ss << std::hex << hash1;
    ss << std::hex << hash2;
    return ss.str();
}

