/* 
 * File:   TCPPacket.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 2:06 AM
 * 
 * Represents a captured UDP packet
 */

#ifndef UDPPACKET_H
#define UDPPACKET_H

#include <vector>
#include <pcap/pcap.h>
#include "ippacket.h"
namespace WorldMapper {

    namespace Packet {

        class UDPPacket : public IPPacket {
        public:
            void process(const u_char *&packetptr, const struct pcap_pkthdr* pkthdr, const WorldMapper::Configuration *configuration) override;
            std::string hash128(bool reversed) override;

            u_int8_t getConnectionstate() const {
                return connectionState_;
            }

            u_int16_t getDstport() const {
                return dstport_;
            }

            u_int16_t getSrcport() const {
                return srcport_;
            }

            u_int16_t getUdpLen() const {
                return udpLen_;
            }

        private:
            /**
             * Parses the UDP Header of a pcap package
             * @param packetptr Pointer to the pcap package pointing to the beginning of the udp header
             * @param conn Pointer to the connection where the header informations should be saved to 
             */
            void parseUDP(const u_char *packetptr);

            u_int16_t srcport_;
            u_int16_t dstport_;
            u_int16_t udpLen_;
            u_int8_t connectionState_; /* see Netfilter */





        };
    }
}
#endif /* UDPPACKET_H */

