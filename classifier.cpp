#include <bsoncxx/json.hpp>
#include <mongocxx/uri.hpp>

#include "classifier.h"
#include "external/SpookyV2.h"
#include "external/json.hpp"
#include "worldmapper.h"


using namespace WorldMapper;
using namespace WorldMapper::Packet;
using namespace WorldMapper::Rule;
using json = nlohmann::json;


pthread_mutex_t sessionMutex;
pthread_mutex_t tcpv4Mutex;
pthread_mutex_t udpv4Mutex;

WorldMapper::Classifier::Classifier(WorkingQueue<IPPacket*> *packetQueue, Configuration &config) {
    this->_packetQueue = packetQueue;
    this->_config = config;
    this->_threads = new pthread_t[config.getClassifierThreads()];
    this->_uri = new mongocxx::uri{config.getMongoDBUri()};
    this->_pool = new mongocxx::pool(*_uri);
    _tcpRule = new TCPPacketRule();
    _udpRule = new UDPPacketRule();
}

void WorldMapper::Classifier::run() {
    enforceCloseAll();
    //start worker threads
    int rc;
    int tcnt;

    for (tcnt = 0; tcnt < _config.getClassifierThreads(); tcnt++) {
        cout << "main() : creating thread, " << tcnt << endl;
        rc = pthread_create(&_threads[tcnt], NULL, processPackets, this);
        if (rc) {
            cout << "Error:unable to create thread," << rc << endl;
            exit(-1);
        }
    }
}

bsoncxx::document::value WorldMapper::Classifier::generateTCPSessionSubDocument(TCPPacket * packet) {
    auto builder = bsoncxx::builder::stream::document{};
    auto tcpoptorder = bsoncxx::builder::basic::array{};
    for (auto const &value : packet->getOptorder()) {
        tcpoptorder.append(value);
    }
    return builder
            << "timestamp" << packet->getTs().tv_sec
            << "len" << packet->getLen()
            << "iphl" << packet->getIpHl()
            << "iplen" << packet->getIpLen()
            << "tcpoptions" << tcpoptorder
            << "mss" << packet->getMss()
            << "tos" << packet->getTos()
            << "winsize" << packet->getWsize()
            << "winscale" << packet->getWscale()
            << "tcpheaderlen" << packet->getTcpHl()
            << "ttl" << packet->getTtl()
            << "pktcnt" << 1
            << "datatransfered" << packet->getLen()
            << bsoncxx::builder::stream::finalize;
}

bsoncxx::document::value WorldMapper::Classifier::generateUDPSessionSubDocument(UDPPacket * packet) {
    auto builder = bsoncxx::builder::stream::document{};
    return builder
            << "timestamp" << packet->getTs().tv_sec
            << "len" << packet->getUdpLen()
            << "datatransfered" << packet->getLen()
            << "pktcnt" << 1
            << bsoncxx::builder::stream::finalize;
}

void WorldMapper::Classifier::updateOneMongoDB(bsoncxx::stdx::optional<bsoncxx::document::value> doc, bsoncxx::stdx::optional<bsoncxx::document::value> filter, Collection collection) {

    auto client = _pool->acquire();
    mongocxx::stdx::optional<mongocxx::result::update> result;
    switch (collection) {
        case COLTCPV4:
        {
            pthread_mutex_lock(&tcpv4Mutex);
            result = (*client)[_config.getMongoDBName()][_config.getCollectiontcpIPv4()].update_one(filter->view(), doc->view());
            pthread_mutex_unlock(&tcpv4Mutex);
            break;
        }
        case COLUDPV4:
        {
            pthread_mutex_lock(&udpv4Mutex);
            result = (*client)[_config.getMongoDBName()][_config.getCollectionudpIPv4()].update_one(filter->view(), doc->view());
            pthread_mutex_unlock(&udpv4Mutex);
            break;
        }
            //        case COLTCPV6:
            //        {
            //            pthread_mutex_lock(&tcpv6Mutex);
            //            result = (*client)[DBNAME][_collectionMap[Collection::COLTCPV6]].update_one(filter->view(), doc->view());
            //            pthread_mutex_unlock(&tcpv6Mutex);
            //            break;
            //        }
            //        case COLUDPV6:
            //        {
            //            pthread_mutex_lock(&udpv6Mutex);
            //            result = (*client)[DBNAME][_collectionMap[Collection::COLUDPV6]].update_one(filter->view(), doc->view());
            //            pthread_mutex_unlock(&udpv6Mutex);
            //            break;
            //        }
        default:
            break;
    }
}

bsoncxx::oid WorldMapper::Classifier::insertOneMongoDB(bsoncxx::stdx::optional<bsoncxx::document::value> doc, Collection collection) {
    auto client = _pool->acquire();
    mongocxx::stdx::optional<mongocxx::result::insert_one> result;
    switch (collection) {
        case COLTCPV4:
        {
            pthread_mutex_lock(&tcpv4Mutex);
            result = (*client)[_config.getMongoDBName()][_config.getCollectiontcpIPv4()].insert_one(doc->view());
            pthread_mutex_unlock(&tcpv4Mutex);
            break;
        }
        case COLUDPV4:
        {
            pthread_mutex_lock(&udpv4Mutex);
            result = (*client)[_config.getMongoDBName()][_config.getCollectionudpIPv4()].insert_one(doc->view());
            pthread_mutex_unlock(&udpv4Mutex);
            break;
        }
            //        case COLTCPV6:
            //        {
            //            pthread_mutex_lock(&tcpv6Mutex);
            //            result = (*client)[DBNAME][_collectionMap[Collection::COLTCPV6]].insert_one(doc->view());
            //            pthread_mutex_unlock(&tcpv6Mutex);
            //            break;
            //        }
            //        case COLUDPV6:
            //        {
            //            pthread_mutex_lock(&udpv6Mutex);
            //            result = (*client)[DBNAME][_collectionMap[Collection::COLUDPV6]].insert_one(doc->view());
            //            pthread_mutex_unlock(&udpv6Mutex);
            //            break;
            //        }
        default:
            break;
    }

    return result->inserted_id().get_oid().value;
}

void WorldMapper::Classifier::enforceClose(Session session, Collection collection) {

    string oid = session.identifier;
    bsoncxx::oid boid(oid);
    //remove from active sessions list
    pthread_mutex_lock(&sessionMutex);
    sessions.erase(oid);
    pthread_mutex_unlock(&sessionMutex);

    bsoncxx::stdx::optional<bsoncxx::document::value> doc;
    auto builder = bsoncxx::builder::stream::document{};
    doc = builder << "$set"
            << bsoncxx::builder::stream::open_document
            << "active" << 0
            << bsoncxx::builder::stream::close_document
            << bsoncxx::builder::stream::finalize;
    auto filter = builder << "_id" << boid
            << bsoncxx::builder::stream::finalize;

    updateOneMongoDB(doc, filter, collection);
}

void WorldMapper::Classifier::enforceCloseAll() {
    bsoncxx::stdx::optional<bsoncxx::document::value> doc;
    auto builder = bsoncxx::builder::stream::document{};
    doc = builder << "$set"
            << bsoncxx::builder::stream::open_document
            << "active" << 0
            << bsoncxx::builder::stream::close_document
            << bsoncxx::builder::stream::finalize;
    auto filter = builder << "active" << 1
            << bsoncxx::builder::stream::finalize;
    auto client = _pool->acquire();
    mongocxx::stdx::optional<mongocxx::result::update> result;
    pthread_mutex_lock(&tcpv4Mutex);
    cout << "closing all tcp" << endl;
    result = (*client)[_config.getMongoDBName()][_config.getCollectiontcpIPv4()].update_many(filter.view(), doc->view());
    pthread_mutex_unlock(&tcpv4Mutex);

    pthread_mutex_lock(&udpv4Mutex);
    result = (*client)[_config.getMongoDBName()][_config.getCollectionudpIPv4()].update_many(filter.view(), doc->view());
    pthread_mutex_unlock(&udpv4Mutex);
}

void WorldMapper::Classifier::writeTCPPacketToMongoDB(TCPPacket * tcppacket) {
    auto builder = bsoncxx::builder::stream::document{};
    // hash for the two possible session identifiers, depending on node role
    string clienthash = tcppacket->hash128(false);
    string serverhash = tcppacket->hash128(true);

    //find session by hash
    auto session = sessions.find(clienthash);
    Collection col;
    if (tcppacket->getIpv() == 4) {
        col = Collection::COLTCPV4;
    } else {
        //        col = Collection::COLTCPV6;
        return;
    }

    //First SYN packet
    if (tcppacket->getSyn() == 1 && tcppacket->getAck() == 0) {
        //session is already open -> missed close?
        if (session != sessions.end()) {
            enforceClose(session->second, col);
        }
        //tcp session document
        auto docValue = builder
                << "clienthash" << clienthash
                << "serverhash" << serverhash
                << "srcip" << tcppacket->getSrcip().toString()
                << "srcport" << tcppacket->getSrcport()
                << "dstip" << tcppacket->getDstip().toString()
                << "dstport" << tcppacket->getDstport()
                << "active" << 1
                << "proto" << tcppacket->getProto()
                << "client" << generateTCPSessionSubDocument(tcppacket)
                << "direction" << tcppacket->getPacketDirection()
                << bsoncxx::builder::stream::finalize;

        //insert session document
        bsoncxx::oid oid = insertOneMongoDB(docValue, col);
        pthread_mutex_lock(&sessionMutex);
        //store document id for server and client hash with kind specification
        sessions[serverhash] = Session{oid.to_string(), 1, tcppacket->getTs(), tcppacket->getTs(), false};
//        cout << serverhash <<" val: " << sessions[serverhash].sc << endl;
        sessions[clienthash] = Session{oid.to_string(), 0, tcppacket->getTs(), tcppacket->getTs(), false};
        pthread_mutex_unlock(&sessionMutex);
    }//Follow up packets
    else {
        string role = "server";
        //determine if packet origin is client or server 
        string oid = "";
        if (session != sessions.end()) {
            oid = session->second.identifier;
            //determine the actual packet direction according to the first SYN packet seen
            if (session->second.sc == 0) {
//                cout << serverhash << " srcip: " << tcppacket->getSrcip().toString() << " srcport: " << tcppacket->getSrcport() << endl;
                role = "client";
            }
        } else {
            return;
        }
        sessions[serverhash].updated = tcppacket->getTs();
        sessions[clienthash].updated = tcppacket->getTs();
        bsoncxx::stdx::optional<bsoncxx::document::value> doc;
        //create filter
        bsoncxx::oid boid(oid);
        auto filter = builder << "_id" << boid
                << bsoncxx::builder::stream::finalize;

        //SYN ACK
        if (tcppacket->getSyn() == 1 && tcppacket->getAck() == 1) {
            doc = builder << "$set"
                    << bsoncxx::builder::stream::open_document
                    << "server" << generateTCPSessionSubDocument(tcppacket)
                    << bsoncxx::builder::stream::close_document
                    << bsoncxx::builder::stream::finalize;
        } else {

            //increment packet count
            auto update = builder << "$inc"
                    << bsoncxx::builder::stream::open_document
                    << role + ".pktcnt" << 1
                    << role + ".datatransfered" << tcppacket->getLen()
                    << bsoncxx::builder::stream::close_document;

            //in case of session shut down change state of session in db and remove from sessions list
            if (tcppacket->getFin() || tcppacket->getRst()) {
                update = update << "$set"
                        << bsoncxx::builder::stream::open_document
                        << "active" << 0
                        << bsoncxx::builder::stream::close_document;
                //remove from active sessions list
                pthread_mutex_lock(&sessionMutex);
                sessions.erase(oid);
                pthread_mutex_unlock(&sessionMutex);
            }
            doc = update << bsoncxx::builder::stream::finalize;
        }

        updateOneMongoDB(doc, filter, col);
    }
}

void WorldMapper::Classifier::writeUDPPacketToMongoDB(UDPPacket * udppacket) {
    auto builder = bsoncxx::builder::stream::document{};
    // hash for the two possible session identifiers, depending on node role
    string clienthash = udppacket->hash128(false);
    string serverhash = udppacket->hash128(true);

    //find session by hash
    auto session = sessions.find(serverhash);
    Collection col;
    if (udppacket->getIpv() == 4) {
        col = Collection::COLUDPV4;
    } else {
        //        col = Collection::COLUDPV6;
        return;
    }

    //check if a matching session already exists
    if (session != sessions.end()) {
        //check if session may has timed out
        if ((udppacket->getTs().tv_sec - session->second.updated.tv_sec) > _config.getUDPTimeout()) {
            enforceClose(session->second, col);
        } else {

            string role = "server";
            //determine if packet origin is client or server 
            string oid = "";
            if (session != sessions.end()) {
                oid = session->second.identifier;
                //determine the actual session direction according to the first packet seen
                if (session->second.sc == 1) {
                    role = "client";
                }
            } else {
                return;
            }

            sessions[serverhash].updated = udppacket->getTs();
            sessions[clienthash].updated = udppacket->getTs();

            bsoncxx::oid boid(oid);
            auto filter = builder << "_id" << boid
                    << bsoncxx::builder::stream::finalize;
            bsoncxx::stdx::optional<bsoncxx::document::value> doc;
            if (session->second.sc == 0) {
                doc = builder << "$set"
                        << bsoncxx::builder::stream::open_document
                        << "server" << generateUDPSessionSubDocument(udppacket)
                        << bsoncxx::builder::stream::close_document
                        << bsoncxx::builder::stream::finalize;
            } else {

                //increment packet count
                doc = builder << "$inc"
                        << bsoncxx::builder::stream::open_document
                        << role + ".pktcnt" << 1
                        << role + ".datatransfered" << udppacket->getLen()
                        << bsoncxx::builder::stream::close_document
                        << bsoncxx::builder::stream::finalize;
            }
            updateOneMongoDB(doc, filter, col);
        }

    } else {
        //tcp session document
        auto docValue = builder
                << "clienthash" << clienthash
                << "serverhash" << serverhash
                << "srcip" << udppacket->getSrcip().toString()
                << "srcport" << udppacket->getSrcport()
                << "dstip" << udppacket->getDstip().toString()
                << "dstport" << udppacket->getDstport()
                << "active" << 1
                << "proto" << udppacket->getProto()
                << "client" << generateUDPSessionSubDocument(udppacket)
                << "direction" << udppacket->getPacketDirection()
                << bsoncxx::builder::stream::finalize;

        //insert session document
        bsoncxx::oid oid = insertOneMongoDB(docValue, col);
        //store document id for server and client hash with kind specification
        pthread_mutex_lock(&sessionMutex);
        //store document id for server and client hash with kind specification
        sessions[serverhash] = Session{oid.to_string(), 0, udppacket->getTs(), udppacket->getTs()};
        sessions[clienthash] = Session{oid.to_string(), 1, udppacket->getTs(), udppacket->getTs()};
        pthread_mutex_unlock(&sessionMutex);
    }

}

void WorldMapper::Classifier::classificationToMongo(classificationMap classification, IPPacket *packet, Collection collection) {
    bsoncxx::builder::basic::document doc;
    map<Category, bsoncxx::builder::basic::array> atr;
    for (const auto &cat : classification) {
        auto catArrB = bsoncxx::builder::basic::array{};
        for (const auto &classi : cat.second) {
            auto valArrB = bsoncxx::builder::basic::array{};
            for (const auto &val : classi) {
                valArrB.append(val);
            }
            catArrB.append(valArrB);
        }
        doc.append(kvp(categoryMap.find(cat.first)->second, catArrB));

    }
    std::cout << "ip:" << packet->getSrcip().toString() << " :: " << bsoncxx::to_json(doc) << std::endl;
    auto builder = bsoncxx::builder::stream::document{};
    string hash = packet->hash128(false);
    auto sessionid = sessions.find(hash);
    string oid = "";

    if (sessionid != sessions.end()) {
        oid = sessionid->second.identifier;
    } else {
        std::cout << "oops we shouldn't reach here (classificationToMongo)" << std::endl;
        return;
    }

    //create filter
    bsoncxx::oid boid(oid);
    auto filter = builder << "_id" << boid
            << bsoncxx::builder::stream::finalize;
    bsoncxx::stdx::optional<bsoncxx::document::value> update;
    update = builder << "$set"
            << bsoncxx::builder::stream::open_document
            << "classification" << doc
            << bsoncxx::builder::stream::close_document
            << bsoncxx::builder::stream::finalize;


    //Update the document
    updateOneMongoDB(update, filter, collection);

}

void *WorldMapper::Classifier::processPackets(void *p) {
    static_cast<Classifier*> (p)->process();
}

void WorldMapper::Classifier::process() {
    while (_run) {
        IPPacket *packet = (IPPacket *) _packetQueue->pop();
        classificationMap classification;
        string clienthash = packet->hash128(false);
        string serverhash = packet->hash128(true);

        switch (packet->getProto()) {
            case IPPROTO_TCP:
            {
                classificationMap classification;
                TCPPacket *tcppacket = (TCPPacket *) packet;
                writeTCPPacketToMongoDB(tcppacket);
                auto session = sessions.find(serverhash);
                if (session != sessions.end()) {
                    if (session->second.classified == false) {
                        if (_tcpRule->apply(tcppacket, classification)) {
                            Collection collection;
                            if (packet->getIpv() == 4) {
                                collection = Collection::COLTCPV4;
                            } else {
                                //                                collection = Collection::COLTCPV6;
                                continue;
                            }
                            classificationToMongo(classification, packet, collection);
                            sessions[serverhash].classified = true;
                            sessions[clienthash].classified = true;
                        }
                    }
                }

                break;
            }

            case IPPROTO_UDP:
            {
                UDPPacket *udppacket = (UDPPacket *) packet;
                writeUDPPacketToMongoDB(udppacket);

                auto session = sessions.find(serverhash);
                if (session != sessions.end()) {
                    if (session->second.classified == false) {
                        if (_udpRule->apply(udppacket, classification)) {
                            Collection collection;
                            if (packet->getIpv() == 4) {
                                collection = Collection::COLUDPV4;
                            } else {
                                //                                collection = Collection::COLUDPV6;
                                continue;
                            }
                            classificationToMongo(classification, packet, collection);
                            sessions[serverhash].classified = true;
                            sessions[clienthash].classified = true;
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
    }
}

