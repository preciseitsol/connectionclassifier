#include "util.h"
#include <sstream>

std::vector<std::string> WorldMapper::split(std::string s, const char delimiter) {
    size_t start = 0;
    size_t end = s.find_first_of(delimiter);

    std::vector<std::string> output;

    if (s.empty() == false) {

        while (end <= std::string::npos) {
            output.emplace_back(s.substr(start, end - start));

            if (end == std::string::npos)
                break;

            start = end + 1;
            end = s.find_first_of(delimiter, start);
        }

    }
    return output;
}