/* 
 * File:   Configuration.cpp
 * Author: Gerald Ortner g.ortner@gmail.com
 * 
 * Created on July 5, 2017, 2:43 PM
 */

#include <fstream>
#include <Poco/Net/IPAddress.h>
#include <Poco/Exception.h>
#include "external/json.hpp"

#include "configuration.h"
#include "network.h"

using json = nlohmann::json;

WorldMapper::Configuration::Configuration() {
    this->readConfiguration();
}

std::vector<WorldMapper::Network> WorldMapper::Configuration::getLocalIPRanges()const {
    return this->_localIPRanges;
}

std::vector<Poco::Net::IPAddress> WorldMapper::Configuration::getExternalIPs()const {
    return this->_externalIPs;
}

int WorldMapper::Configuration::getTCPTimeout() const {
    return this->_tcpTimeout;
}

int WorldMapper::Configuration::getUDPTimeout() const {
    return this->_udpTimeout;
}

std::string WorldMapper::Configuration::getMongoDBUri() const {
    return this->_mongoDBUri;
}

std::string WorldMapper::Configuration::getMongoDBName() const {
    return this->_mongoDBName;
}

std::string WorldMapper::Configuration::getCollectiontcpIPv4() const {
    return this->_collectiontcpIPv4;
}

std::string WorldMapper::Configuration::getCollectionudpIPv4() const {
    return this->_collectionudpIPv4;
}

int WorldMapper::Configuration::getClassifierThreads() const {
    return this->_classifierThreads;
}

void WorldMapper::Configuration::readConfiguration() {
    std::ifstream i("config/configuration");
    if (i) // Verify that the file was open successfully
    {
        json j;
        i >> j;

        if (j.find("localIPRanges") != j.end()) {
            json o = j.find("localIPRanges").value();
            if (o.is_array()) {
                for (json::iterator it = o.begin(); it != o.end(); ++it) {
                    std::string cdir = it.value();
                    int cdirLoc = cdir.find("/");
                    auto ip = Poco::Net::IPAddress(cdir.substr(0, cdirLoc));
                    int mask = stoi(cdir.substr(cdirLoc + 1, cdir.length()));
                    auto netmask = Poco::Net::IPAddress(mask, ip.family());
                    _localIPRanges.push_back({ip, netmask});
                }
            }
        }
        if (j.find("externalIPs") != j.end()) {
            json o = j.find("externalIPs").value();
            if (o.is_array()) {
                for (json::iterator it = o.begin(); it != o.end(); ++it) {
                    std::string ips = it.value();
                    auto ip = Poco::Net::IPAddress(ips);
                    _externalIPs.push_back(ip);
                }
            }
        }
        if (j.find("tcpTimeout") != j.end()) {
            json o = j.find("tcpTimeout").value();
            if (o.is_number_integer()) {
                _tcpTimeout = o.front();
            }
        }
        if (j.find("udpTimeout") != j.end()) {
            json o = j.find("tcpTimeout").value();
            if (o.is_number_integer()) {
                _udpTimeout = o.front();
            }
        }
        if (j.find("mongoDBUri") != j.end()) {
            json o = j.find("mongoDBUri").value();
            if (o.is_string()) {
                _mongoDBUri = o.front();
            }
        }
        if (j.find("mongoDBName") != j.end()) {
            json o = j.find("mongoDBName").value();
            if (o.is_string()) {
                _mongoDBName = o.front();
            }
        }
        if (j.find("collectiontcpIPv4") != j.end()) {
            json o = j.find("collectiontcpIPv4").value();
            if (o.is_string()) {
                _collectiontcpIPv4 = o.front();
            }
        }
        if (j.find("collectionudpIPv4") != j.end()) {
            json o = j.find("collectionudpIPv4").value();
            if (o.is_string()) {
                _collectionudpIPv4 = o.front();
            }
        }
        if (j.find("classifierThreads") != j.end()) {
            json o = j.find("classifierThreads").value();
            if (o.is_number_integer()) {
                _classifierThreads = o.front();
            }
        }
        i.close();
    } else {
        std::cerr << "File could not be opened!" << std::endl; // Report error
        std::cerr << "Error code: " << strerror(errno) << std::endl; // Get some info as to why
    }
}