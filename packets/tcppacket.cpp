/* 
 * File:   TCPPacket.cpp
 * Author: Gerald Ortner g.ortner@gmail.com
 * 
 * Created on June 10, 2017, 2:06 AM
 */

#include <netinet/tcp.h>
#include <sstream>
#include <iostream>

#include "tcppacket.h"
#include "../external/SpookyV2.h"

using namespace std;

void WorldMapper::Packet::TCPPacket::process(const u_char *&packetptr, const struct pcap_pkthdr* pkthdr, const WorldMapper::Configuration *configuration) {
    WorldMapper::Packet::IPPacket::process(packetptr, pkthdr, configuration);
    struct tcphdr* tcph = (struct tcphdr*) packetptr;
    this->ack_ = tcph->ack;
    this->urg_ = tcph->urg;
    this->psh_ = tcph->psh;
    this->rst_ = tcph->rst;
    this->syn_ = tcph->syn;
    this->fin_ = tcph->fin;
    this->wSize_ = ntohs(tcph->window);
    this->tcpHl_ = tcph->doff;
    this->srcport_ = ntohs(tcph->source);
    this->dstport_ = ntohs(tcph->dest);
    parseTCPOptions(tcph);

}

std::string WorldMapper::Packet::TCPPacket::hash128(bool reversed) {

    uint64 hash1 = 1;
    uint64 hash2 = 2;
    char *inBuf = new char[BUFFLEN]();
    if (reversed) {
        snprintf(inBuf, BUFFLEN, "%s%u%u%s%u", this->dstip_.toString().c_str(), this->dstport_
                , this->proto_, this->srcip_.toString().c_str(), this->srcport_);
    } else {
        snprintf(inBuf, BUFFLEN, "%s%u%u%s%u", this->srcip_.toString().c_str(), this->srcport_
                , this->proto_, this->dstip_.toString().c_str(), this->dstport_);
    }

    SpookyHash::Hash128(inBuf, BUFFLEN, &hash1, &hash2);

    std::stringstream ss;
    ss << std::hex << hash1;
    ss << std::hex << hash2;
    return ss.str();
}

/**
 * Parses the TCP Header Options field
 * @param tcph Pointer to the TCPHeader part of the pcap package
 * @param conn Pointer to the connection where the option informations should be saved to
 */
void WorldMapper::Packet::TCPPacket::parseTCPOptions(struct tcphdr *tcph) {
    int length = (tcph->doff * 4) - sizeof (struct tcphdr);
    const u_char *ptr = (const u_char *) (tcph + 1);

    //run through all options
    while (length > 0) {
        int opcode = *ptr++;
        int opsize;

        switch (opcode) {
            case TCPOPT_EOL:
                return;
            case TCPOPT_NOP: /* Ref: RFC 793 section 3.1 */
                length--;
                this->optorder_.emplace_back(TCPOPT_NOP);
                continue;
            default:
                opsize = *ptr++;
                if (opsize < 2) /* "wrong options" */
                    return;
                if (opsize > length)
                    return; /* don't parse partial options */
                switch (opcode) {
                    case TCPOPT_MAXSEG:
                        if (opsize == TCPOLEN_MAXSEG && tcph->syn) {
                            this->mss_ = ntohs(*(uint16_t *) ptr);
                        }
                        this->optorder_.emplace_back(TCPOPT_MAXSEG);
                        break;
                    case TCPOPT_WINDOW:
                        if (opsize == TCPOLEN_WINDOW && tcph->syn) {
                            this->wScale_ = *(uint8_t *) ptr;
                            //scale must be <=14                        
                        }
                        this->optorder_.emplace_back(TCPOPT_WINDOW);
                        break;
                    case TCPOPT_TIMESTAMP:
                        if (opsize == TCPOLEN_TIMESTAMP) {
                            uint8_t tstamppresent = 1;
                            this->tsVal_ = htonl(*(uint32_t *) ptr);
                            this->tsEcR_ = htonl(*(uint32_t *) (ptr + 4));
                            //printf("tsval: %u,tsecr: %u  \n", tsval, tsecr);
                        }
                        this->optorder_.emplace_back(TCPOPT_TIMESTAMP);
                        break;
                    case TCPOPT_SACK_PERMITTED:
                        if (opsize == TCPOLEN_SACK_PERMITTED && tcph->syn) {
                            this->sackP_ = 1;
                        }
                        this->optorder_.emplace_back(TCPOPT_SACK_PERMITTED);
                        break;
                }
                ptr += opsize - 2;
                length -= opsize;
        }
    }
}

