/* 
 * File:   TCPIPPacket.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 7, 2017, 12:51 AM
 * 
 * Represents a captured IP packet
 */

#ifndef TCPIPPACKET_H
#define TCPIPPACKET_H

#include <netinet/in.h>
#include <string>
#include <stdlib.h>
#include <pcap/pcap.h>
#include <Poco/Net/IPAddress.h>
#include <Poco/Exception.h>
#include "../configuration.h"

namespace WorldMapper {

    namespace Packet {

        enum PacketDirection {
            IN,
            OUT,
            BOTH
        };

        class IPPacket {
        public:

            const int BUFFLEN = 255;

            virtual void process(const u_char *&packetptr, const struct pcap_pkthdr* pkthdr, const WorldMapper::Configuration *configuration) = 0;

            /**
             * Creates a 128bit connection identifier Hash for the packet
             * @param reversed Specifies if source and destination should be inverted
             * @return The 128 bit hash value
             */
            virtual std::string hash128(bool reversed);

            const WorldMapper::Packet::PacketDirection getPacketDirection();

            const Poco::Net::IPAddress getDstip() const;

            int getLen() const;

            u_int8_t getIpHl() const;

            unsigned int getIpv() const;

            u_short getIpLen() const;

            u_int8_t getProto() const;

            u_int8_t getProtocol() const;

            Poco::Net::IPAddress getSrcip() const;

            u_int8_t getTos() const;

            timeval getTs() const;

            u_int8_t getTtl() const;

        protected:
            PacketDirection direction_;
            struct timeval ts_;
            Poco::Net::IPAddress srcip_;
            Poco::Net::IPAddress dstip_;
            int len_; /* packet length */
            u_int8_t ipv_;
            u_int8_t ipHl_; /* header length */
            u_int8_t tos_; /* type of service */
            u_short ipLen_; /* total length */
            u_int8_t ttl_; /* time to live */
            u_int8_t protocol_; /* protocol */
            u_int8_t proto_; /*IP protocol*/
        };
    }
}
#endif /* TCPIPPACKET_H */

