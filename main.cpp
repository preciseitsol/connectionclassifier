
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "worldmapper.h"

const int BUFFLEN = 255;


using namespace std;

/**
 * Main Function
 * @param argc Number of arguments
 * @param argv Arguments
 * @return 
 */
int main(int argc, char** argv) {
    char interface[BUFFLEN] = "", bpfstr[BUFFLEN] = "";
    int packets = 0, c, i;

    // Get the command line options, if any
    while ((c = getopt(argc, argv, "hi:n:")) != -1) {
        switch (c) {
            case 'h':
                printf("usage: %s [-h] [-i ] [-n ] []\n", argv[0]);
                exit(0);
                break;
            case 'i':
                snprintf(interface, BUFFLEN, "%s", optarg);
                break;
            case 'n':
                packets = atoi(optarg);
                break;
        }
    }
    //Get the packet capture filter expression, if any.
    for (i = optind; i < argc; i++) {
        snprintf(bpfstr, BUFFLEN, "%s ", argv[i]);
    }

    WorldMapper::WorldMapper(interface, bpfstr);
    exit(0);
}

