/* 
 * File:   Configuration.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on July 5, 2017, 2:43 PM
 */

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "network.h"

namespace WorldMapper {

    class Configuration {
    public:
        Configuration();

        std::vector<WorldMapper::Network> getLocalIPRanges() const;
        std::vector<Poco::Net::IPAddress> getExternalIPs() const;
        int getTCPTimeout() const;
        int getUDPTimeout() const;
        std::string getMongoDBUri() const;
        std::string getMongoDBName() const;
        std::string getCollectiontcpIPv4() const;
        std::string getCollectionudpIPv4() const;
        int getClassifierThreads() const;

    private:
        void readConfiguration();
        std::vector<WorldMapper::Network> _localIPRanges;
        std::vector<Poco::Net::IPAddress> _externalIPs;
        int _tcpTimeout;
        int _udpTimeout;
        std::string _mongoDBUri;
        std::string _mongoDBName;
        std::string _collectiontcpIPv4;
        std::string _collectionudpIPv4;
        int _classifierThreads;
        
    };
}

#endif /* CONFIGURATION_H */

