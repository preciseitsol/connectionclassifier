/* 
 * File:   TORRules.cpp
 * Author:  Gerald Ortner g.ortner@gmail.com
 * 
 * Created on June 10, 2017, 3:58 PM
 */

#include <iostream>
#include <cerrno>
#include <cstring>
#include <resolv.h>
#include <netdb.h>
#include <sstream>

#include "torrule.h"
#include "../network.h"
#include "../packets/tcppacket.h"
#define N 4096

bool WorldMapper::Rule::TORRules::apply(const WorldMapper::Packet::IPPacket *packet, classificationMap &classification) const {
    WorldMapper::Packet::TCPPacket* tcpp = (WorldMapper::Packet::TCPPacket *) packet;

    //we only want to classify incoming connections
    if (tcpp->getPacketDirection() == WorldMapper::Packet::PacketDirection::OUT
            || (tcpp->getSyn() == 1 && tcpp->getAck() == 0) == false) {
        return false;
    }
    Configuration config;

    for (auto ip : config.getExternalIPs()) {
        //reverse the ip address segment order
        std::string revSrc = WorldMapper::reverseIP(packet->getSrcip());
        std::string revDst = WorldMapper::reverseIP(ip);

        //build query string
        std::stringstream query;
        query << revSrc << "." << tcpp->getDstport() << "." << revDst << ".ip-port.exitlist.torproject.org";
        std::cout << query.str() << std::endl;

        u_char nsbuf[N];
        char dispbuf[N];
        ns_msg msg;
        ns_rr rr;

        int length = res_query(query.str().c_str(), C_IN, T_A, nsbuf, sizeof (nsbuf));
        if (length < 0) {
            continue;
        }

        HEADER *hdr = reinterpret_cast<HEADER*> (nsbuf);
        //NXDOMAIN is returned if no matching TOR exit node is found
        if (hdr->rcode == NXDOMAIN) {
            continue;
        }

        int k = ns_initparse(nsbuf, length, &msg);
        if (k == -1) {
            std::cerr << errno << " " << strerror(errno) << "\n";
            continue;
        }

        //number of returned address records
        int addrrecords = ntohs(hdr->ancount);
        k = ns_parserr(&msg, ns_s_an, 0, &rr);
        if (k == -1) {
            std::cerr << errno << " " << std::strerror(errno) << "\n";
            continue;
        }
        const u_char *data = ns_rr_rdata(rr);

        unsigned int addr = ns_get32(data);
        struct in_addr in;
        in.s_addr = ntohl(addr);
        char *a = inet_ntoa(in);

        //127.0.0.2 is returned if there is a tor exit node matching the srcip.dstprt.dstip
        if (strcmp(a, "127.0.0.2") == 0) {
            classification[Category::SERVICE].push_back({"TOR"});
            return true;
        }
    }

    return false;
}


