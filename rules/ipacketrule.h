/* 
 * File:   rule.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 12:47 AM
 * 
 * Packet rule interface
 */

#ifndef RULE_H
#define RULE_H


#include <iostream>
#include <vector>
#include <map>
#include <ctime>
#include <Poco/Net/IPAddress.h>
#include <Poco/Exception.h>

#include "../packets/ippacket.h"
#include "../configuration.h"

namespace WorldMapper {

    enum Category {
        SERVICE, OS, NETWORKT, SERVICEPROVIDER
    };

    const std::map<Category,std::string > categoryMap = {
        {SERVICE, "SERVICE"},
        {NETWORKT, "NETWORKT"},
        {SERVICEPROVIDER, "SERVICEPROVIDER"}
    };

    /*
     * Map which holds classifications for each rules per category
     */
    typedef std::map<Category, std::vector<std::string>> ruleClassificationsMap;

    /*
     * Map which holds all classifications for all matching rules per category
     */
    typedef std::map<Category, std::vector< std::vector<std::string>>> classificationMap;
    namespace Rule {

        class IPacketRule {
        public:
            virtual bool apply(const Packet::IPPacket *packet, classificationMap &classification) const = 0;
            std::string name;
            Poco::Net::IPAddress ip;
            Poco::Net::IPAddress netmask;
            ruleClassificationsMap classifications;

        };
    }
}
#endif /* RULE_H */

