/* 
 * File:   TCPIPPacket.cpp
 * Author: Gerald Ortner g.ortner@gmail.com
 * 
 * Created on June 7, 2017, 12:51 AM
 */
#include <iostream>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sstream>
#include "../external/SpookyV2.h"


#include "ippacket.h"

using namespace Poco::Net;

void WorldMapper::Packet::IPPacket::process(const u_char *&packetptr, const struct pcap_pkthdr* pkthdr, const WorldMapper::Configuration *configuration) {
    this->ts_ = pkthdr->ts;
    struct ip *iphdr = (struct ip*) packetptr;
    char srcip[INET6_ADDRSTRLEN], dstip[INET6_ADDRSTRLEN];

    inet_ntop(AF_INET, &iphdr->ip_src.s_addr, srcip, sizeof (srcip));
    inet_ntop(AF_INET, &iphdr->ip_dst.s_addr, dstip, sizeof (dstip));
    this->len_ = pkthdr->len;
    this->srcip_ = IPAddress(srcip);
    this->dstip_ = IPAddress(dstip);
    this->ipv_ = 4;
    this->ipHl_ = 4 * iphdr->ip_hl;
    this->ipLen_ = iphdr->ip_len;
    this->tos_ = iphdr->ip_tos;
    this->ttl_ = iphdr->ip_ttl;
    this->proto_ = iphdr->ip_p;
    
    this->direction_ == PacketDirection::IN;
    for (auto &net : configuration->getLocalIPRanges()) {
        if ((this->srcip_ & net.netmask) == net.ip) {
            this->direction_ = PacketDirection::OUT;
            break;
        }
    }
    // Advance to the transport layer header
    packetptr += 4 * iphdr->ip_hl;
}

std::string WorldMapper::Packet::IPPacket::hash128(bool reversed) {
    uint64 hash1 = 1;
    uint64 hash2 = 2;
    char *inBuf = new char[BUFFLEN]();
    if (reversed) {
        snprintf(inBuf, BUFFLEN, "%s%u%s", this->dstip_.toString().c_str(), this->proto_
                , this->srcip_.toString().c_str());
    } else {
        snprintf(inBuf, BUFFLEN, "%s%u%s", this->srcip_.toString().c_str(), this->proto_
                , this->dstip_.toString().c_str());
    }

    SpookyHash::Hash128(inBuf, BUFFLEN, &hash1, &hash2);

    std::stringstream ss;
    ss << std::hex << hash1;
    ss << std::hex << hash2;
    return ss.str();
}

const WorldMapper::Packet::PacketDirection WorldMapper::Packet::IPPacket::getPacketDirection() {
    return this->direction_;
}

int WorldMapper::Packet::IPPacket::getLen() const {
    return this->len_;
}

const Poco::Net::IPAddress WorldMapper::Packet::IPPacket::getDstip() const {
    return this->dstip_;
}

u_int8_t WorldMapper::Packet::IPPacket::getIpHl() const {
    return ipHl_;
}

unsigned int WorldMapper::Packet::IPPacket::getIpv() const {
    return ipv_;
}

u_short WorldMapper::Packet::IPPacket::getIpLen() const {
    return ipLen_;
}

/**
 * IP Header Protocol
 * @return 
 */
u_int8_t WorldMapper::Packet::IPPacket::getProto() const {
    return proto_;
}

u_int8_t WorldMapper::Packet::IPPacket::getProtocol() const {
    return protocol_;
}

Poco::Net::IPAddress WorldMapper::Packet::IPPacket::getSrcip() const {
    return srcip_;
}

u_int8_t WorldMapper::Packet::IPPacket::getTos() const {
    return tos_;
}

timeval WorldMapper::Packet::IPPacket::getTs() const {
    return ts_;
}

u_int8_t WorldMapper::Packet::IPPacket::getTtl() const {
    return ttl_;
}
