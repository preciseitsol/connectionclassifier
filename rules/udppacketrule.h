/* 
 * File:   TCPPacketRule.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 1:33 AM
 * 
 * Applies UDP rules to the given packet
 */

#ifndef UDPPACKETRULE_H
#define UDPPACKETRULE_H

#include <vector>

#include "ipacketrule.h"
#include "../packets/udppacket.h"

namespace WorldMapper {
    namespace Rule {

        class UDPPacketRule : public IPacketRule {
        public:
            UDPPacketRule();
            bool apply(const Packet::IPPacket *packet, classificationMap &classification) const override;
        private:
            std::vector<IPacketRule*> rules_;

        };
    }
}
#endif /* UDPPACKETRULE_H */

