
#include <signal.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/if_ether.h>
#include <netinet/ip_icmp.h>

#include "collector.h"

using namespace WorldMapper::Packet;
using namespace WorldMapper;

std::string WorldMapper::Collector::_interface = "";
std::string WorldMapper::Collector::_captureFilter = "";
int WorldMapper::Collector::_packets = 0;
pcap_t* WorldMapper::Collector::_pd;
bool WorldMapper::Collector::_run;
int WorldMapper::Collector::_linkhdrlen = 0;
WorkingQueue<WorldMapper::Packet::IPPacket*> *WorldMapper::Collector::_packetQueue;
Configuration WorldMapper::Collector::_config;

void WorldMapper::Collector::run(std::string interface, std::string captureFilter
        , WorkingQueue<WorldMapper::Packet::IPPacket*> * packetQueue, Configuration &config) {
    _config = config;
    _packetQueue = packetQueue;
    WorldMapper::Collector::_interface = interface;
    _captureFilter = captureFilter;
    if ((_pd = openPcapSocket(_interface.c_str(), _captureFilter.c_str()))) {
        signal(SIGINT, signalHandling);
        signal(SIGTERM, signalHandling);
        signal(SIGQUIT, signalHandling);
        captureLoop(_pd, _packets, (pcap_handler) pcapCallback);
        signalHandling(0);
    }
}

void WorldMapper::Collector::signalHandling(int signo) {
    _run = false;
    struct pcap_stat stats;

    if (pcap_stats(_pd, &stats) >= 0) {
        printf("%u packets received\n", stats.ps_recv);
        printf("%u packets dropped\n\n", stats.ps_drop);
    }
    pcap_close(_pd);
    exit(0);
}

u_int16_t WorldMapper::Collector::etherPacket(u_char *args, const struct pcap_pkthdr *pkthdr, const u_char *p) {
    struct ether_header *eptr = (struct ether_header*) p;
    assert(pkthdr->caplen <= pkthdr->len);
    assert(pkthdr->caplen >= sizeof (struct ether_header));
    return eptr->ether_type;
}

pcap_t* WorldMapper::Collector::openPcapSocket(const char* device, const char* bpfstr) {
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* pd;
    uint32_t srcip, netmask;
    struct bpf_program bpf;

    // If no network interface (device) is specfied, get the first one.
    if (!*device && !(device = pcap_lookupdev(errbuf))) {
        printf("pcap_lookupdev(): %s\n", errbuf);
        return NULL;
    }

    // Open the device for live capture, as opposed to reading a packet
    // capture file.
    if ((pd = pcap_open_live(device, BUFSIZ, 1, 0, errbuf)) == NULL) {
        printf("pcap_open_live(): %s\n", errbuf);
        return NULL;
    }

    // Get network device source IP address and netmask.
    if (pcap_lookupnet(device, &srcip, &netmask, errbuf) < 0) {
        printf("pcap_lookupnet: %s\n", errbuf);
        return NULL;
    }

    // Convert the packet filter epxression into a packet
    // filter binary.
    if (pcap_compile(pd, &bpf, (char*) bpfstr, 0, netmask)) {
        printf("pcap_compile(): %s\n", pcap_geterr(pd));
        return NULL;
    }

    // Assign the packet filter to the given libpcap socket.
    if (pcap_setfilter(pd, &bpf) < 0) {
        printf("pcap_setfilter(): %s\n", pcap_geterr(pd));
        return NULL;
    }

    return pd;
}

void WorldMapper::Collector::captureLoop(pcap_t* pd, int packets, pcap_handler func) {
    int linktype;

    // Determine the datalink layer type.
    if ((linktype = pcap_datalink(pd)) < 0) {
        printf("pcap_datalink(): %s\n", pcap_geterr(pd));
        return;
    }

    // Set the datalink layer header size.
    switch (linktype) {
        case DLT_NULL:
            _linkhdrlen = 4;
            break;

        case DLT_EN10MB:
            _linkhdrlen = 14;
            break;

        case DLT_SLIP:
        case DLT_PPP:
            _linkhdrlen = 24;
            break;

        default:
            printf("Unsupported datalink (%d)\n", linktype);
            return;
    }

    // Start capturing packets.
    if (pcap_loop(pd, packets, func, 0) < 0) {
        printf("pcap_loop failed: %s\n", pcap_geterr(pd));
    }
}

void WorldMapper::Collector::parseIP(const u_char *packetptr, const struct pcap_pkthdr* pkthdr) {
    struct ip *iphdr = (struct ip*) packetptr;
    switch (iphdr->ip_p) {
        case IPPROTO_TCP:
        {
            TCPPacket *packet = new TCPPacket();
            bool srcinternal = false;
            bool dstinternal = false;
            packet->process(packetptr, pkthdr, &_config);
            //ignore internal traffic
            Poco::Net::IPAddress srcip = packet->getSrcip();
            Poco::Net::IPAddress dstip = packet->getDstip();

            if ((srcip.isBroadcast() || srcip.isLoopback() || srcip.isSiteLocal()
                    || srcip.isLinkLocal() || srcip.isSiteLocal()) &&
                    (dstip.isBroadcast() || dstip.isLoopback() || dstip.isGlobalMC()
                    || dstip.isSiteLocal() || dstip.isLinkLocal() || dstip.isSiteLocal())) {
                return;
            }


            for (auto &net : _config.getLocalIPRanges()) {
                srcip.mask(net.netmask);
                dstip.mask(net.netmask);

                if (srcip == net.ip) {
                    srcinternal = true;
                }
                if (dstip == net.ip) {
                    dstinternal = true;
                }
                if (srcinternal && dstinternal) {

                    return;
                }

            }
            _packetQueue->push(packet);
            break;
        }
        case IPPROTO_UDP:
        {
            UDPPacket *packet = new UDPPacket();
            bool srcinternal = false;
            bool dstinternal = false;
            //ignore internal traffic
            packet->process(packetptr, pkthdr, &_config);

            Poco::Net::IPAddress srcip = packet->getSrcip();
            Poco::Net::IPAddress dstip = packet->getDstip();

            if ((srcip.isBroadcast() || srcip.isLoopback() || srcip.isGlobalMC()
                    || srcip.isSiteLocal() || srcip.isLinkLocal() || srcip.isSiteLocal())
                    &&(dstip.isBroadcast() || dstip.isLoopback() || dstip.isGlobalMC()
                    || dstip.isSiteLocal() || dstip.isLinkLocal() || dstip.isSiteLocal())) {
                return;
            }

            for (auto &net : _config.getLocalIPRanges()) {

                srcip.mask(net.netmask);
                dstip.mask(net.netmask);
                if (srcip == net.ip) {
                    srcinternal = true;
                }
                if (dstip == net.ip) {
                    dstinternal = true;
                }
                if (srcinternal && dstinternal) {

                    return;
                }

            }

            _packetQueue->push(packet);

            break;
        }
        default:
            //ignore everything else
            break;
    }
}

void WorldMapper::Collector::pcapCallback(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* p) {

    const u_int16_t type = etherPacket(args, pkthdr, p);
    //step over ethernet header
    p += sizeof (struct ethhdr);

    switch (ntohs(type)) {
        case ETH_P_IP:
            parseIP(p, pkthdr);
            break;
        case ETH_P_IPV6:
            //not implemented yet
            break;
    }
}



