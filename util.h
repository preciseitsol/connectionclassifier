/* 
 * File:   util.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on July 10, 2017, 12:18 AM
 * 
 * Helper class for various functions
 */

#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <vector>

namespace WorldMapper {
    std::vector<std::string> split(std::string s, const char delimiter);
}
#endif /* UTIL_H */

