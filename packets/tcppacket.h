/* 
 * File:   TCPPacket.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 2:06 AM
 * 
 * Represents a captured TCP packet
 */

#ifndef TCPPACKET_H
#define TCPPACKET_H

#include <vector>
#include <pcap/pcap.h>
#include "ippacket.h"
namespace WorldMapper {

    namespace Packet {

        class TCPPacket : public IPPacket {
        public:
            void process(const u_char *&packetptr, const struct pcap_pkthdr* pkthdr, const WorldMapper::Configuration *configuration) override;
            std::string hash128(bool reversed) override;

            u_int16_t getAck() const {
                return ack_;
            }

            u_int8_t getConnectionstate() const {
                return connectionState_;
            }

            u_int16_t getDstport() const {
                return dstport_;
            }

            u_int16_t getFin() const {
                return fin_;
            }

            u_int16_t getTcpHl() const {
                return tcpHl_;
            }

            u_int16_t getMss() const {
                return mss_;
            }

            std::vector<int> getOptorder() const {
                return optorder_;
            }

            void appendToOptorder(int val) {
                optorder_.push_back(val);
            }

            u_int16_t getPsh() const {
                return psh_;
            }

            u_int16_t getRst() const {
                return rst_;
            }

            u_int16_t getSackp() const {
                return sackP_;
            }

            u_int16_t getSrcport() const {
                return srcport_;
            }

            u_int16_t getSyn() const {
                return syn_;
            }

            uint32_t getTsecr() const {
                return tsEcR_;
            }

            uint8_t getTspresent() const {
                return tsPresent_;
            }

            uint32_t getTsval() const {
                return tsVal_;
            }

            u_int16_t getUrg() const {
                return urg_;
            }

            uint8_t getWscale() const {
                return wScale_;
            }

            u_int16_t getWsize() const {
                return wSize_;
            }
        private:
            /**
             * Parses the TCP Header of a pcap package
             * @param packetptr Pointer to the pcap package pointing to the beginning of the tcp header
             * @param conn Pointer to the connection where the header informations should be saved to 
             */
            void parseTCP(const u_char *packetptr);

            /**
             * Parses the TCP Header Options field
             * @param tcph Pointer to the TCPHeader part of the pcap package
             * @param conn Pointer to the connection where the option informations should be saved to
             */
            void parseTCPOptions(struct tcphdr *tcph);

            u_int16_t srcport_;
            u_int16_t dstport_;
            u_int8_t connectionState_; /* see Netfilter */
            u_int16_t wSize_;
            uint8_t wScale_;
            u_int16_t tcpHl_; /*header length */
            u_int16_t mss_; /*max segment size */
            u_int16_t sackP_; /*SACK permitted*/

            uint8_t tsPresent_; /* Timestamp exists */
            uint32_t tsVal_; /*Timestamp value*/
            uint32_t tsEcR_; /*Timestamp Echo Reply*/

            u_int16_t urg_;
            u_int16_t ack_;
            u_int16_t psh_;
            u_int16_t rst_;
            u_int16_t syn_;
            u_int16_t fin_;

            std::vector<int> optorder_;



        };
    }
}
#endif /* TCPPACKET_H */

