/* 
 * File:   network.h
 * Author: Gerald Ortner g.ortner@gmail.com
 *
 * Created on July 5, 2017, 2:44 PM
 * 
 * Helper class for network related operations
 */

#ifndef NETWORK_H
#define NETWORK_H

#include <string.h>
#include <Poco/Net/IPAddress.h>

namespace WorldMapper {

    struct Network {
        Poco::Net::IPAddress ip;
        Poco::Net::IPAddress netmask;
    };

    std::string reverseIP(Poco::Net::IPAddress ip);
}



#endif /* NETWORK_H */

