/* 
 * File:   SignatureRules.cpp
 * Author:  Gerald Ortner g.ortner@gmail.com
 * 
 * Created on June 10, 2017, 3:57 PM
 */

#include "ipsignaturerule.h"

#include <iostream>
#include <fstream>
#include "Poco/DirectoryIterator.h"
#include "Poco/File.h"

using Poco::DirectoryIterator;
using Poco::Path;
using Poco::File;

WorldMapper::Rule::IPSignatureRule::IPSignatureRule() {
}

bool WorldMapper::Rule::IPSignatureRule::apply(const WorldMapper::Packet::IPPacket *packet, classificationMap &classification) const {
    try {
        for (auto rule : rules) {
            Poco::Net::IPAddress srcip = packet->getSrcip();
            srcip.mask(rule.netmask);
            if (rule.ip == srcip) {
                for (auto const& s : rule.classifications) {
                    classification[s.first].emplace_back(s.second);
                }
                return true;
            }
        }
    } catch(Poco::AssertionViolationException &e){
        std::cerr << "assertion error ipsignature rule:" << e.what() << std::endl; 
    }
    return false;

}

void WorldMapper::Rule::IPSignatureRule::readRules() {
    std::vector<std::string> rulePaths;
    std::string cwd("config/ip.rules.d");
    DirectoryIterator it(cwd);
    DirectoryIterator end;
    while (it != end) {
        if (it->isFile()) {
            Path p(it->path(), Path::PATH_UNIX);
            if (p.getExtension().compare("rules") == 0) {
                readRulesFromFile(it->path());
            }
        }
        ++it;
    }
}

void WorldMapper::Rule::IPSignatureRule::readRulesFromFile(std::string path) {
    std::ifstream i(path);
    if (i) // Verify that the file was opened successfully
    {
        json j;
        i >> j;

        if (j.find("IPRulesIPv4") != j.end()) {
            json o = j.find("IPRulesIPv4").value();
            if (o.is_array()) {
                for (json::iterator it = o.begin(); it != o.end(); ++it) {
                    IPSignatureRule rule = *it;
                    rules.push_back(rule);
                }

            }
        }
        i.close();
    } else {
        std::cerr << "File could not be opened!" << std::endl; // Report error
        std::cerr << "Error code: " << strerror(errno) << std::endl; // Get some info as to why
    }
}