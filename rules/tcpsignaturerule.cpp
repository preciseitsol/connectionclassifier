/* 
 * File:   SignatureRules.cpp
 * Author:  Gerald Ortner g.ortner@gmail.com
 * 
 * Created on June 10, 2017, 3:57 PM
 */

#include "tcpsignaturerule.h"

#include <error.h>
#include <iostream>
#include <fstream>
#include <ctype.h>

#include "Poco/DirectoryIterator.h"
#include "Poco/File.h"
#include "../packets/tcppacket.h"

using Poco::DirectoryIterator;
using Poco::Path;
using Poco::File;

WorldMapper::Rule::TCPSignatureRule::TCPSignatureRule() {
}

bool WorldMapper::Rule::TCPSignatureRule::apply(const WorldMapper::Packet::IPPacket *packet, classificationMap &classification) const {
    WorldMapper::Packet::TCPPacket *tcppacket = (WorldMapper::Packet::TCPPacket *) packet;

    int bestmatch = 0;
    TCPSignatureRule matchRule;
    for (auto rule : rules) {
        //only apply rule if packet direction matches
        if (rule.direction != tcppacket->getPacketDirection()) {
            continue;
        }
        int matchcnt = 0;
        bool mismatch = false;
        for (auto &option : rule.options) {
            if (mismatch) {
                break;
            }
            try {
                switch (tcpRuleOptionsMap.at(option.first)) {
                    case SRCPRT:
                        if (doesMatch(option.second, tcppacket->getSrcport())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case DSTPRT:
                        if (doesMatch(option.second, tcppacket->getDstport())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case WSIZE:
                        if (doesMatch(option.second, tcppacket->getWsize())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case WSCALE:
                        if (doesMatch(option.second, tcppacket->getWscale())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case TCPHL:
                        if (doesMatch(option.second, tcppacket->getTcpHl())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case MSS:
                        if (doesMatch(option.second, tcppacket->getMss())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case SACKP:
                        if (doesMatch(option.second, tcppacket->getSackp())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case TSPRESENT:
                        if (doesMatch(option.second, tcppacket->getTspresent())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case LEN:
                        if (doesMatch(option.second, tcppacket->getLen())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case IPHL:
                        if (doesMatch(option.second, tcppacket->getIpHl())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case IPLEN:
                        if (doesMatch(option.second, tcppacket->getIpLen())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case TOS:
                        if (doesMatch(option.second, tcppacket->getTos())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                    case PROTO:
                        if (doesMatch(option.second, tcppacket->getProto())) {
                            matchcnt++;
                        } else {
                            mismatch = true;
                        }
                        break;
                }
            } catch (std::out_of_range &oor) {
                std::cerr << "Out of Range error: " << oor.what() << " val:" << option.first << '\n';
            }
        }
        if (matchcnt > bestmatch && mismatch == false) {
            bestmatch = matchcnt;
            matchRule = rule;
        }
    }
    if (bestmatch > 0) {
        
        for (auto &clas : matchRule.classifications) {
            classification[clas.first].emplace_back(clas.second);
        }
        return true;
    }

    return false;
}

bool WorldMapper::Rule::TCPSignatureRule::doesMatch(std::vector<int> options, int value) const {
    for (int &option : options) {
        if (option != value) {
            return false;
        }
    }
    return true;
}

void WorldMapper::Rule::TCPSignatureRule::readRules() {

    std::vector<std::string> rulePaths;
    std::string cwd("config/tcp.rules.d");
    DirectoryIterator it(cwd);
    DirectoryIterator end;
    while (it != end) {
        if (it->isFile()) {
            Path p(it->path(), Path::PATH_UNIX);
            if (p.getExtension().compare("rules") == 0) {
                readRulesFromFile(it->path());
            }
        }
        ++it;
    }

}

void WorldMapper::Rule::TCPSignatureRule::readRulesFromFile(std::string path) {
    std::ifstream i(path);
    if (i) // Verify that the file was open successfully
    {
        json j;
        i >> j;

        if (j.find("TCPRulesIPv4") != j.end()) {
            json o = j.find("TCPRulesIPv4").value();
            if (o.is_array()) {
                for (json::iterator it = o.begin(); it != o.end(); ++it) {
                    TCPSignatureRule rule = *it;
                    rules.push_back(rule);
                }
            }
        }
        i.close();
    } else {
        std::cerr << "File could not be opened!" << std::endl; // Report error
        std::cerr << "Error code: " << strerror(errno) << std::endl; // Get some info as to why
    }
}