/* 
 * File:   TORRules.h
 * Author:  Gerald Ortner g.ortner@gmail.com
 *
 * Created on June 10, 2017, 3:58 PM
 * 
 * Applies TOR ruls to given packets
 */

#ifndef TORRULES_H
#define TORRULES_H

#include <udns.h> 
#include "ipacketrule.h"
#include "../packets/ippacket.h"

namespace WorldMapper {
    namespace Rule {

        class TORRules : public IPacketRule {
        public:
            bool apply(const WorldMapper::Packet::IPPacket *packet, classificationMap &Connclassificationclassification) const override;
        };
    }
}
#endif /* TORRULES_H */

